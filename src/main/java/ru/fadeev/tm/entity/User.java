package ru.fadeev.tm.entity;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.enumerated.Role;

import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@Getter
@Setter
@XmlType
@NoArgsConstructor
@ToString(callSuper=true)
public final class User extends AbstractEntity  implements Serializable {

    private static final long serialVersionUID = -7402071995321582277L;

    @Nullable
    private String name ="";

    @Nullable
    private String password = "";

    @NotNull
    private Role role = Role.USER;

}