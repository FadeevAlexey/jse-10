package ru.fadeev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.command.AbstractCommand;

import java.util.LinkedHashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
public class AppState {

    @Nullable
    private User user;

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

}