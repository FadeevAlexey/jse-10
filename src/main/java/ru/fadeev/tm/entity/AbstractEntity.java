package ru.fadeev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@ToString
public abstract class AbstractEntity implements Serializable {

    private static final long serialVersionUID = -5719993878615755420L;

    @NotNull
    @XmlElement
    private final String id = UUID.randomUUID().toString();

    @NotNull
    public String getId() {
        return id;
    }

}