package ru.fadeev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class HashUtil {

    @NotNull
    public static String stringToMd5Hash(@Nullable final String password) {
        if (password == null || password.isEmpty())
            return "";
        @NotNull final String saltSuffix = "e~w2Dd";
        @NotNull final String saltPrefix = "P$73if";
        @NotNull StringBuilder md5Hash = new StringBuilder(saltPrefix + password + saltSuffix);
        try {
           @NotNull final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            for (int i = 0; i < 60000; i++) {
                messageDigest.update(md5Hash.toString().getBytes());
                byte[] digest = messageDigest.digest();
                md5Hash = new StringBuilder(saltPrefix + DatatypeConverter.printHexBinary(digest) + saltSuffix);
            }
        } catch (final NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return md5Hash.toString().substring(saltPrefix.length(), md5Hash.length() - saltSuffix.length());
    }

}