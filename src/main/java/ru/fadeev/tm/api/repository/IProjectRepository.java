package ru.fadeev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Project;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void removeAll(@NotNull String userId);

    @NotNull
    List<Project> findAll(@NotNull String userId);

    @Nullable
    String findIdByName(@NotNull String name, @NotNull String userId);

    @NotNull
    Collection<Project> findAll(@NotNull String userId, @NotNull Comparator<? super Project> selectedSort);

    @NotNull
    Collection<Project> searchByName(@NotNull String userId, @NotNull String string);

    @NotNull
    Collection<Project> searchByDescription(@NotNull String userId, @NotNull String string);

}
