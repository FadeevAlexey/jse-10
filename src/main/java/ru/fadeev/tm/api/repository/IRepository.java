package ru.fadeev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IRepository<E> {

    @NotNull
    List<E> findAll();

    @NotNull
    E findOne(@NotNull String id);

    @NotNull
    E remove(@NotNull String id);

    @Nullable
    E persist(@NotNull E e);

    @Nullable
    E merge(@NotNull E e);

    void removeAll();

}