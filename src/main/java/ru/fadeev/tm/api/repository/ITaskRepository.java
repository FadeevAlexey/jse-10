package ru.fadeev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Task;

import java.util.Collection;
import java.util.Comparator;

public interface ITaskRepository extends IRepository<Task> {

    void removeAll(@NotNull String userId);

    @NotNull
    Collection<Task> findAll(@NotNull String userId);

    @NotNull
    Collection<Task> findAllByProjectId(@NotNull String projectId, @NotNull String userId);

    @Nullable
    String findIdByName(@NotNull String name, @NotNull String userId);

    void removeAllByProjectId(@NotNull String projectId, @NotNull String userId);

    void removeAllProjectTask(@NotNull String userId);

    @NotNull
    Collection<Task> findAll(@NotNull String userId, @NotNull Comparator<? super Task> selectedSort);

    @NotNull
    Collection<Task> searchByName(@NotNull String userId, @NotNull String string);

    @NotNull
    Collection<Task> searchByDescription(@NotNull String userId, @NotNull String string);

}