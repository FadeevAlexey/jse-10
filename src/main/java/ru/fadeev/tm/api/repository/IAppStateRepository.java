package ru.fadeev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;

import java.util.List;

public interface IAppStateRepository {

    @Nullable
    Role getRole();

    void putCommand(@Nullable String description, @Nullable AbstractCommand abstractCommand);

    @Nullable
    AbstractCommand getCommand(@Nullable String command);

    @NotNull
    List<AbstractCommand> getCommands();

    boolean hasPermission(@NotNull Role... roles);

    @Nullable
    String getUserId();

    @Nullable
    User getUser();

    void setUser(@Nullable User user);

}