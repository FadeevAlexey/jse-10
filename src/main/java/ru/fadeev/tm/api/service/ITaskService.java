package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Task;

import java.util.Collection;
import java.util.Comparator;

public interface ITaskService extends IService<Task> {

    void removeAll(@Nullable String userId);

    @NotNull
    Collection<Task> findAll(@Nullable String userId);

    @Nullable
    Collection<Task> findAllByProjectId(@Nullable String projectId, @Nullable String userId);

    @Nullable
    String findIdByName(@Nullable String name, @Nullable String userId);

    void removeAllByProjectId(@Nullable String projectId, @Nullable String userId);

    void removeAllProjectTask(@Nullable String userId);

    @NotNull
    Collection<Task> findAll(@NotNull String userId, @Nullable Comparator<? super Task> selectedSort);

    @NotNull
    Collection<Task> searchByName(@Nullable String userId, @Nullable String string);

    @NotNull
    Collection<Task> searchByDescription(@Nullable String userId, @Nullable String string);

}