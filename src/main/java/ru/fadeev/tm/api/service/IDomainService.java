package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Domain;

public interface IDomainService {

    void saveData(@Nullable final Domain domain);

    void loadData(@Nullable final Domain domain);

}