package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;

import java.util.List;

public interface IAppStateService {

    @Nullable
    Role getRole();

    @Nullable
    User getUser();

    void setUser(@Nullable User user);

    void putCommand(@Nullable String description, @Nullable AbstractCommand abstractCommand);

    @Nullable
    AbstractCommand getCommand(@Nullable String command);

    @NotNull
    List<AbstractCommand> getCommands();

    boolean hasPermission(@Nullable Role...roles);

    @Nullable
    String getUserId();

}
