package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.entity.Task;

import java.text.ParseException;
import java.util.Collection;
import java.util.Date;

public interface ITerminalService {

    @Nullable
    String readString();

    @Nullable
    Date readDate() throws ParseException;

    @NotNull
    String dateToString(@Nullable Date date);

    void println(@Nullable String string);

    void print(@Nullable String string);

    void println(@Nullable Task task);

    void println(@Nullable Project project);

    void printTaskList(@Nullable Collection<Task> tasks);

    void printProjectList(@Nullable Collection<Project> projects);

}
