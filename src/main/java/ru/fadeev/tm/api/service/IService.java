package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IService<T> {

    @NotNull
    List<T> findAll();

    @Nullable
    T findOne(@Nullable String id);

    @Nullable
    T remove(@Nullable String id);

    @Nullable
    T persist(@Nullable T t);

    @Nullable
    T merge(@Nullable T t);

    void removeAll();

}