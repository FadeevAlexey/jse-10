package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IAppStateService getAppStateService();

    @NotNull
    ITerminalService getTerminalService();

    @NotNull
    IDomainService getDomainService();

}