package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.IProjectRepository;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.entity.Project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Nullable
    @Override
    public String findIdByName(@Nullable final String name, @Nullable final String userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findIdByName(name, userId);
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        return projectRepository.findAll(userId);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        projectRepository.removeAll(userId);
    }

    @Override
    @NotNull
    public Collection<Project> findAll(@Nullable final String userId, @Nullable final Comparator<? super Project> selectedSort) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        if (selectedSort == null) return findAll(userId);
        return projectRepository.findAll(userId, selectedSort);
    }

    @Override
    public @NotNull Collection<Project> searchByName(@Nullable final String userId, @Nullable final String string) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        if (string == null || string.isEmpty()) return new ArrayList<>();
        return projectRepository.searchByName(userId, string);
    }

    @Override
    public @NotNull Collection<Project> searchByDescription(@Nullable final String userId, @Nullable final String string) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        if (string == null || string.isEmpty()) return new ArrayList<>();
        return projectRepository.searchByDescription(userId, string);
    }

}