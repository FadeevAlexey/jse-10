package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.api.repository.ITaskRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        taskRepository.removeAll(userId);
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        return taskRepository.findAll(userId);
    }

    @Override
    public @Nullable Collection<Task> findAllByProjectId(@Nullable String projectId, @Nullable String userId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return taskRepository.findAllByProjectId(projectId,userId);
    }

    @Nullable
    @Override
    public String findIdByName(@Nullable final String name, @Nullable final String userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return taskRepository.findIdByName(name,userId);
    }



    @Override
    public void removeAllByProjectId(@Nullable final String projectId, @Nullable final String userId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        taskRepository.removeAllByProjectId(projectId,userId);
    }

    @Override
    public void removeAllProjectTask(@Nullable final String userId){
        if (userId == null || userId.isEmpty()) return;
        taskRepository.removeAllProjectTask(userId);
    }

    @NotNull
    @Override
    public Collection<Task> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<? super Task> selectedSort) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        if (selectedSort == null) return findAll(userId);
        return taskRepository.findAll(userId,selectedSort);
    }

    @NotNull
    @Override
    public Collection<Task> searchByName(@Nullable final String userId, @Nullable final String string) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        if (string == null || string.isEmpty()) return new ArrayList<>();
        return taskRepository.searchByName(userId,string);
    }

    @NotNull
    @Override
    public Collection<Task> searchByDescription(@Nullable final String userId, @Nullable final String string) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        if (string == null || string.isEmpty()) return new ArrayList<>();
        return taskRepository.searchByDescription(userId,string);
    }

}