package ru.fadeev.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.IDomainService;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.entity.Domain;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.exception.DomainException;

import java.util.Collections;

@RequiredArgsConstructor
public final class DomainService implements IDomainService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IProjectService projectService;

    @Override
    public void saveData(@Nullable final Domain domain) {
        if (domain == null) throw new DomainException("Domain can't be empty");
        domain.setUsers(userService.findAll());
        domain.setTasks(taskService.findAll());
        domain.setProjects(projectService.findAll());
    }

    @Override
    public void loadData(@Nullable final Domain domain) {
        if (domain == null) throw new DomainException("Domain can't be empty");
        if (domain.getUsers() == null) domain.setUsers(Collections.emptyList());
        if (domain.getProjects() == null) domain.setProjects(Collections.emptyList());
        if (domain.getTasks() == null) domain.setTasks(Collections.emptyList());
        for (User user: domain.getUsers()) userService.persist(user);
        for (Task task: domain.getTasks()) taskService.persist(task);
        for (Project project: domain.getProjects()) projectService.persist(project);
    }

}