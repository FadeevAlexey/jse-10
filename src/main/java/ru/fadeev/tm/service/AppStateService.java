package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.api.repository.IAppStateRepository;

import java.util.*;

public class AppStateService  implements IAppStateService {

    private final IAppStateRepository appStateRepository;

    public AppStateService(IAppStateRepository appStateRepository) {
        this.appStateRepository = appStateRepository;
    }

    @Override
    @Nullable
    public Role getRole() {
        return appStateRepository.getRole();
    }

    @Override
    @Nullable
    public User getUser() {
        return appStateRepository.getUser();
    }

    @Override
    public void setUser(@Nullable final User user) {
        appStateRepository.setUser(user);
    }

    @Override
    public void putCommand(@Nullable final String description, @Nullable final AbstractCommand abstractCommand) {
        if (description == null || description.isEmpty()) return;
        if (abstractCommand == null) return;
        appStateRepository.putCommand(description, abstractCommand);
    }

    @Override
    @Nullable
    public AbstractCommand getCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) return null;
        return appStateRepository.getCommand(command);
    }

    @Override
    @NotNull
    public List<AbstractCommand> getCommands() {
        return appStateRepository.getCommands();
    }

    @Override
    public boolean hasPermission(@Nullable Role... roles) {
        if (roles == null) return false;
        return appStateRepository.hasPermission();
    }

    @Override
    @Nullable
    public String getUserId() {
        return appStateRepository.getUserId();
    }

}