package ru.fadeev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.IProjectRepository;
import ru.fadeev.tm.entity.Project;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public void removeAll(@NotNull final String userId) {
        findAll().stream()
                .filter(project -> userId.equals(project.getUserId()))
                .forEach(project -> remove(project.getId()));
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        return findAll().stream()
                .filter(project -> userId.equals(project.getUserId()))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public String findIdByName(@NotNull final String name, @NotNull final String userId) {
        @NotNull final Optional<Project> optionalProject = super.findAll()
                .stream()
                .filter(project ->
                        name.equals(project.getName()) && userId.equals(project.getUserId()))
                .findAny();
        return optionalProject.map(Project::getId).orElse(null);
    }

    @NotNull
    public Collection<Project> findAll(@NotNull final String userId, @NotNull final Comparator<? super Project> selectedSort) {
        return findAll(userId).stream()
                .sorted(selectedSort)
                .collect(Collectors.toList());
    }

    @NotNull
    public Collection<Project> searchByName(@NotNull final String userId, @NotNull final String string){
        return findAll(userId).stream()
                .filter(project -> project.getName() != null && project.getName().contains(string))
                .collect(Collectors.toList());
    }

    @NotNull
    public Collection<Project> searchByDescription(@NotNull final String userId, @NotNull final String string){
        return findAll(userId).stream()
                .filter(project -> project.getDescription() != null && project.getDescription().contains(string))
                .collect(Collectors.toList());
    }

}