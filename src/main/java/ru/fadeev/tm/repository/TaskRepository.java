package ru.fadeev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.ITaskRepository;
import ru.fadeev.tm.entity.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void removeAll(@NotNull final String userId) {
        findAll().stream()
                .filter(task -> userId.equals(task.getUserId()))
                .forEach(task -> remove(task.getId()));
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@NotNull final String userId) {
        return findAll().stream()
                .filter(task ->userId.equals(task.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<Task> findAllByProjectId(@NotNull final String projectId, @NotNull final String userId) {
        return findAll(userId).stream()
                .filter(task -> projectId.equals(task.getProjectId()))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public String findIdByName(@NotNull final String name, @NotNull final String userId) {
       @NotNull final Optional<Task> optionalProject = super.findAll()
                .stream()
                .filter(task ->
                        name.equals(task.getName()) && userId.equals(task.getUserId()))
                .findAny();
        return optionalProject.map(Task::getId).orElse(null);
    }

    @Override
    public void removeAllByProjectId(@NotNull final String projectId, @NotNull final String userId) {
        findAll(userId)
                .stream()
                .filter(task -> projectId.equals(task.getProjectId()))
                .forEach(task -> remove(task.getId()));
    }

    @Override
    public void removeAllProjectTask(@NotNull final String userId) {
        findAll(userId)
                .forEach(task -> {
                    if (task.getProjectId() != null && !task.getProjectId().isEmpty())
                        remove(task.getId());
                });
    }

    @NotNull
    public Collection<Task> findAll(
            @NotNull final String userId,
            @NotNull final Comparator<? super Task> selectedSort
    ) {
        return findAll(userId).stream()
                .sorted(selectedSort)
                .collect(Collectors.toList());
    }

    @NotNull
    public Collection<Task> searchByName(@NotNull final String userId, @NotNull final String string){
        return findAll(userId).stream()
                .filter(task -> task.getName() != null && task.getName().contains(string))
                .collect(Collectors.toList());
    }

    @NotNull
    public Collection<Task> searchByDescription(@NotNull final String userId, @NotNull final String string){
        return findAll(userId).stream()
                .filter(task -> task.getDescription() != null && task.getDescription().contains(string))
                .collect(Collectors.toList());
    }

}