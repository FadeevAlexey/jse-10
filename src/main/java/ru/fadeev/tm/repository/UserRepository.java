package ru.fadeev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.IUserRepository;
import ru.fadeev.tm.entity.User;

public final class UserRepository extends AbstractRepository<User>  implements IUserRepository {

    @Override
    public boolean isLoginExist(@NotNull final String login){
      return findAll().stream()
                .anyMatch(user -> login.equals(user.getName()));
    }

    @Override
    public @Nullable User findUserByLogin(@NotNull String login) {
        return findAll().stream()
                .filter(usr -> login.equals(usr.getName()))
                .findFirst().orElse(null);
    }

}