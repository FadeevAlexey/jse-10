package ru.fadeev.tm.exception;

import org.jetbrains.annotations.Nullable;

public final class IllegalDateException extends RuntimeException {

    public IllegalDateException(@Nullable final String message) {
        super(message);
    }

}
