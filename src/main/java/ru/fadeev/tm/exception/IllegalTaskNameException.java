package ru.fadeev.tm.exception;

import org.jetbrains.annotations.Nullable;

public final class IllegalTaskNameException extends RuntimeException {

    public IllegalTaskNameException(@Nullable final String message) {
        super(message);
    }

}
