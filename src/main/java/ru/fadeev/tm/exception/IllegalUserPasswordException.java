package ru.fadeev.tm.exception;

import org.jetbrains.annotations.Nullable;

public final class IllegalUserPasswordException extends RuntimeException {

    public IllegalUserPasswordException(@Nullable final String message) {
        super(message);
    }

}
