package ru.fadeev.tm.command.data;

import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.Domain;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;

import javax.naming.spi.ObjectFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public final class DataJsonJaxbLoadFile extends AbstractCommand {

    @NotNull
    final Path dataFolder = Paths.get(System.getProperty("user.dir") + File.separator + "data");

    @NotNull
    final Path dataPath = Paths.get(dataFolder + File.separator + "jaxb.json");

    @NotNull
    @Override
    public String getName() {
        return "data-json-jaxb-load";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "load data form json format file (JAXB)";
    }

    @Override
    public void execute() throws IOException, JAXBException {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        terminal.println("[LOAD DATA FROM JSON FORMAT FILE (JAXB)]");
        if (!Files.exists(dataPath)) throw new FileNotFoundException("File can't find");
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(
                new Class[]{Domain.class, ObjectFactory.class},
                getProperties()
        );
        @NotNull final Unmarshaller unMarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unMarshaller.unmarshal(dataPath.toFile());
        serviceLocator.getAppStateService().setUser(null);
        clearUsersData();
        serviceLocator.getDomainService().loadData(domain);
        terminal.println("You will be logged out of the application");
        terminal.println("[OK]\n");
    }

    @NotNull
    private Map<String, Object> getProperties() {
        @NotNull final Map<String, Object> properties = new HashMap<>();
        properties.put(JAXBContextProperties.MEDIA_TYPE, "application/json");
        properties.put(JAXBContextProperties.JSON_INCLUDE_ROOT, true);
        return properties;
    }

    public void clearUsersData() {
        serviceLocator.getTaskService().removeAll();
        serviceLocator.getProjectService().removeAll();
        serviceLocator.getUserService().removeAll();
    }

    @Override
    public boolean isPermission(@Nullable final User user) {
        if (user == null) return false;
        return user.getRole() == Role.ADMINISTRATOR;
    }

}