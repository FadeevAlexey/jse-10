package ru.fadeev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.Domain;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class DataBinarySaveFile extends AbstractCommand {

    @NotNull
    final Path dataFolder = Paths.get(System.getProperty("user.dir") + File.separator + "data");

    @NotNull
    final Path dataPath = Paths.get(dataFolder + File.separator +"data.bin");

    @NotNull
    @Override
    public String getName() {
        return "data-binary-save";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "save data to binary format file";
    }

    @Override
    public void execute() throws IOException {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        terminal.println("[SAVE DATA TO BINARY FORMAT FILE]");
        if (!Files.exists(dataFolder)) Files.createDirectory(dataFolder);
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainService().saveData(domain);
        try (
               @NotNull final FileOutputStream fileOutput = new FileOutputStream(dataPath.toFile());
               @NotNull final ObjectOutputStream outputStream = new ObjectOutputStream(fileOutput);
        ) {
            outputStream.writeObject(domain);
        }
        terminal.println("[OK]\n");
    }

    @Override
    public boolean isPermission(@Nullable final User user) {
        if (user == null) return false;
        return user.getRole() == Role.ADMINISTRATOR;
    }

}