package ru.fadeev.tm.command.data;

import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.Domain;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;

import javax.naming.spi.ObjectFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public final class DataJsonJaxbSaveFile extends AbstractCommand {

    @NotNull
    final Path dataFolder = Paths.get(System.getProperty("user.dir") + File.separator + "data");

    @NotNull
    final Path dataPath = Paths.get(dataFolder + File.separator + "jaxb.json");

    @NotNull
    @Override
    public String getName() {
        return "data-json-jaxb-save";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "save data to Json format file (JAXB)";
    }

    @Override
    public void execute() throws IOException, JAXBException {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        terminal.println("[SAVE DATA TO JSON FORMAT FILE]");
        if (!Files.exists(dataFolder)) Files.createDirectory(dataFolder);
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainService().saveData(domain);
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(
                new Class[]{Domain.class, ObjectFactory.class},
                getProperties()
        );
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.marshal(domain,dataPath.toFile());
        terminal.println("[OK]\n");
    }

    @NotNull
    private Map<String, Object> getProperties(){
        @NotNull final Map<String,Object> properties = new HashMap<>();
        properties.put(JAXBContextProperties.MEDIA_TYPE, "application/json");
        properties.put(JAXBContextProperties.JSON_INCLUDE_ROOT, false);
        return properties;
    }

    @Override
    public boolean isPermission(@Nullable final User user) {
        if (user == null) return false;
        return user.getRole() == Role.ADMINISTRATOR;
    }

}