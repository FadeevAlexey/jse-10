package ru.fadeev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.Domain;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class DataXmlJaxbSaveFile extends AbstractCommand {

    @NotNull
    final Path dataFolder = Paths.get(System.getProperty("user.dir") + File.separator + "data");

    @NotNull
    final Path dataPath = Paths.get(dataFolder + File.separator + "jaxb.xml");

    @NotNull
    @Override
    public String getName() {
        return "data-xml-jaxb-save";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "save data to XML format file (JAXB)";
    }

    @Override
    public void execute() throws IOException, JAXBException {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        terminal.println("[SAVE DATA TO XML FORMAT FILE]");
        if (!Files.exists(dataFolder)) Files.createDirectory(dataFolder);
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainService().saveData(domain);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(domain, dataPath.toFile());
        terminal.println("[OK]\n");
    }

    @Override
    public boolean isPermission(@Nullable final User user) {
        if (user == null) return false;
        return user.getRole() == Role.ADMINISTRATOR;
    }

}