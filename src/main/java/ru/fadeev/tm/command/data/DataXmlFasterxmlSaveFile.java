package ru.fadeev.tm.command.data;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.Domain;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class DataXmlFasterxmlSaveFile extends AbstractCommand {

    @NotNull
    final Path dataFolder = Paths.get(System.getProperty("user.dir") + File.separator + "data");

    @NotNull
    final Path dataPath = Paths.get(dataFolder + File.separator + "fasterxml.xml");

    @NotNull
    @Override
    public String getName() {
        return "data-xml-fasterxml-save";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "save data to xml format file (FASTERXML)";
    }

    @Override
    public void execute() throws IOException {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        terminal.println("[SAVE DATA TO XML FORMAT FILE]");
        if (!Files.exists(dataFolder)) Files.createDirectory(dataFolder);
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainService().saveData(domain);
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final ObjectWriter writer = xmlMapper.writerWithDefaultPrettyPrinter();
        writer.writeValue(dataPath.toFile(), domain);
        terminal.println("[OK]\n");
    }

    @Override
    public boolean isPermission(@Nullable final User user) {
        if (user == null) return false;
        return user.getRole() == Role.ADMINISTRATOR;
    }

}