package ru.fadeev.tm.command.data;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.Domain;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class DataXmlFasterxmlLoadFile extends AbstractCommand {

    @NotNull
    final Path dataFolder = Paths.get(System.getProperty("user.dir") + File.separator + "data");

    @NotNull
    final Path dataPath = Paths.get(dataFolder + File.separator + "fasterxml.xml");

    @NotNull
    @Override
    public String getName() {
        return "data-xml-fasterxml-load";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "load data form XML format file (FASTERXML)";
    }

    @Override
    public void execute() throws IOException{
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        terminal.println("[LOAD DATA FROM XML FORMAT FILE (FASTERXML)]");
        if (!Files.exists(dataPath)) throw new FileNotFoundException("File can't find");
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @NotNull final Domain domain = mapper.readValue(dataPath.toFile(), Domain.class);
        serviceLocator.getAppStateService().setUser(null);
        clearUsersData();
        serviceLocator.getDomainService().loadData(domain);
        terminal.println("You will be logged out of the application");
        terminal.println("[OK]\n");
    }

    public void clearUsersData() {
        serviceLocator.getTaskService().removeAll();
        serviceLocator.getProjectService().removeAll();
        serviceLocator.getUserService().removeAll();
    }

    @Override
    public boolean isPermission(@Nullable final User user) {
        if (user == null) return false;
        return user.getRole() == Role.ADMINISTRATOR;
    }

}