package ru.fadeev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.Domain;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class DataXmlJaxbLoadFile extends AbstractCommand {

    @NotNull
    final Path dataFolder = Paths.get(System.getProperty("user.dir") + File.separator + "data");

    @NotNull
    final Path dataPath = Paths.get(dataFolder + File.separator + "jaxb.xml");

    @NotNull
    @Override
    public String getName() {
        return "data-xml-jaxb-load";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "load data form XML format file (JAXB)";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException, JAXBException {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        terminal.println("[LOAD DATA FROM XML FORMAT FILE (JAXB)]");
        if (!Files.exists(dataPath)) throw new FileNotFoundException("File can't find");
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(dataPath.toFile());
        serviceLocator.getAppStateService().setUser(null);
        clearUsersData();
        serviceLocator.getDomainService().loadData(domain);
        terminal.println("You will be logged out of the application");
        terminal.println("[OK]\n");
    }

    public void clearUsersData() {
        serviceLocator.getTaskService().removeAll();
        serviceLocator.getProjectService().removeAll();
        serviceLocator.getUserService().removeAll();
    }

    @Override
    public boolean isPermission(@Nullable final User user) {
        if (user == null) return false;
        return user.getRole() == Role.ADMINISTRATOR;
    }

}