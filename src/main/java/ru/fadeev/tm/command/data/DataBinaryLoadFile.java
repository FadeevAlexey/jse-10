package ru.fadeev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.Domain;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class DataBinaryLoadFile extends AbstractCommand {

    @NotNull
    final Path dataFolder = Paths.get(System.getProperty("user.dir") + File.separator + "data");

    @NotNull
    final Path dataPath = Paths.get(dataFolder + File.separator + "data.bin");

    @NotNull
    @Override
    public String getName() {
        return "data-binary-load";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "load data form binary format file";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        terminal.println("[LOAD DATA FROM BINARY FORMAT FILE]");
        if (!Files.exists(dataPath)) throw new FileNotFoundException("File can't find");
        try (
                @NotNull final FileInputStream fileInputStream = new FileInputStream(dataPath.toFile());
                @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        ) {
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            serviceLocator.getAppStateService().setUser(null);
            clearUsersData();
            serviceLocator.getDomainService().loadData(domain);
        }
        terminal.println("You will be logged out of the application");
        terminal.println("[OK]\n");
    }

    public void clearUsersData() {
        serviceLocator.getTaskService().removeAll();
        serviceLocator.getProjectService().removeAll();
        serviceLocator.getUserService().removeAll();
    }

    @Override
    public boolean isPermission(@Nullable final User user) {
        if (user == null) return false;
        return user.getRole() == Role.ADMINISTRATOR;
    }

}