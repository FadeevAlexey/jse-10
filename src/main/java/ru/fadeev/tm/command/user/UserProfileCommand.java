package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.exception.AccessDeniedException;

public final class UserProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-profile";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show current profile.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final User user = serviceLocator.getAppStateService().getUser();
        if (user == null)
            throw new AccessDeniedException("Access denied");
        terminal.println("[USER PROFILE]");
        terminal.println(String.format(
                "name: %s, role: %s, id: %s",
                user.getName(),user.getRole().getDisplayName(),user.getId()) + "\n");
        terminal.println("IF YOU'D LIKE UPDATE PROFILE USE COMMAND: user-edit\n");
    }

}
