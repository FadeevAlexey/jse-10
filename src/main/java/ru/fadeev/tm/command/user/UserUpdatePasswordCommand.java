package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.exception.IllegalUserNameException;
import ru.fadeev.tm.exception.IllegalUserPasswordException;
import ru.fadeev.tm.util.HashUtil;

public final class UserUpdatePasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-updatePassword";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User Password Changes.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final User currentUser = serviceLocator.getAppStateService().getUser();
        if (currentUser == null)
            throw new IllegalUserNameException("Can't find user");
        terminal.println("[UPDATE PASSWORD]");
        terminal.println("Enter your current password");
        @NotNull final String currentPassword = HashUtil.stringToMd5Hash(terminal.readString());
        if (!currentPassword.equals(currentUser.getPassword()))
            throw new IllegalUserPasswordException("wrong password");
        terminal.println("Enter new password");
        @NotNull final String newPassword = HashUtil.stringToMd5Hash(terminal.readString());
        currentUser.setPassword(newPassword);
        serviceLocator.getUserService().merge(currentUser);
        terminal.println("[OK]\n");
    }

}