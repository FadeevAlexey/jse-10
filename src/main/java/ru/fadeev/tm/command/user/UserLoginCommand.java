package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.exception.IllegalUserNameException;
import ru.fadeev.tm.exception.IllegalUserPasswordException;
import ru.fadeev.tm.util.HashUtil;

public final class UserLoginCommand extends AbstractCommand {

    @Override
    public boolean isPermission(@Nullable final User user) {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "log in task manager";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        terminal.println("[USER LOGIN]");
        terminal.println("ENTER LOGIN");
        @Nullable final String name = terminal.readString();
        @NotNull final boolean loginExist = userService.isLoginExist(name);
        if (!loginExist) throw new IllegalUserNameException("Can't find user");
        @Nullable final User user = userService.findUserByLogin(name);
        if (user == null) throw new IllegalUserNameException("Can't find user");
        terminal.println("ENTER PASSWORD");
        @NotNull final String userPassword = HashUtil.stringToMd5Hash(terminal.readString());
        if (!userPassword.equals(user.getPassword()))
            throw new IllegalUserPasswordException("wrong password, access denied");
        serviceLocator.getAppStateService().setUser(user);
        terminal.println("[OK]\n");
    }

}