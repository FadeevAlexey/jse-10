package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.exception.IllegalUserNameException;
import ru.fadeev.tm.util.HashUtil;

public final class UserCreateCommand extends AbstractCommand {

    @Override
    public boolean isPermission(@Nullable final User user) {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "creates a new user account";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final User currentUser = serviceLocator.getAppStateService().getUser();
        @Nullable final boolean isAdministrator = currentUser != null && Role.ADMINISTRATOR == currentUser.getRole();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        terminal.println("[CREATE ACCOUNT]");
        terminal.println("ENTER NAME");
        @Nullable final String login = terminal.readString();
        @NotNull boolean isLoginExist = userService.isLoginExist(login);
        if (login == null || login.isEmpty()) throw new IllegalUserNameException("Incorrect name");
        if (isLoginExist) throw new IllegalUserNameException("User with same name already exist");
        @NotNull final User user = new User();
        user.setName(login);
        terminal.println("Enter password:");
        user.setPassword(HashUtil.stringToMd5Hash(terminal.readString()));
        if (isAdministrator)
            setRole(user);
        userService.persist(user);
        terminal.println("[OK]\n");
    }

    public void setRole(@NotNull final User user) {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        terminal.println("would you like give new account administrator rights? y/n");
        if ("y".equals(terminal.readString())) user.setRole(Role.ADMINISTRATOR);
    }

}