package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.exception.IllegalUserNameException;
import ru.fadeev.tm.exception.IllegalUserPasswordException;
import ru.fadeev.tm.util.HashUtil;

public final class UserEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit user profile";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User currentUser = serviceLocator.getAppStateService().getUser();
        if (currentUser == null) throw new IllegalUserNameException("Can't find user");
        terminal.println("[EDIT PROFILE]");
        terminal.println("ENTER PASSWORD");
        @NotNull final String password = HashUtil.stringToMd5Hash(terminal.readString());
        if (!password.equals(currentUser.getPassword()))
            throw new IllegalUserPasswordException("wrong password, access denied");
        terminal.println("ENTER NEW NAME OR PRESS ENTER");
        @Nullable final String name = terminal.readString();
        if (userService.isLoginExist(name))
            throw new IllegalUserNameException("User with same name already exist");
        terminal.println("ENTER NEW PASSWORD OR PRESS ENTER");
        @Nullable final String newPassword = terminal.readString();
        if (newPassword != null && !newPassword.isEmpty())
            currentUser.setPassword(HashUtil.stringToMd5Hash(newPassword));
        if (name != null && !name.isEmpty()) currentUser.setName(name);
        userService.merge(currentUser);
        terminal.println("[OK]\n");
    }

}