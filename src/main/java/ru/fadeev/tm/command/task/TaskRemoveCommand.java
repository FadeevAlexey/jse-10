package ru.fadeev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.IllegalProjectNameException;
import ru.fadeev.tm.api.service.ITaskService;

public final class TaskRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected tasks.";
    }

    @Override
    public void execute() {
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        terminal.println("[TASK REMOVE]");
        terminal.println("ENTER NAME");
        @Nullable final String id = taskService.findIdByName(
                terminal.readString(),
                serviceLocator.getAppStateService().getUserId()
        );
        if (id == null) throw new IllegalProjectNameException("Can't find task");
        taskService.remove(id);
        terminal.println("[OK]\n");
    }

}