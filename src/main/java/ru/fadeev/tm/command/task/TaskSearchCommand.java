package ru.fadeev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.IllegalSearchRequestException;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public final class TaskSearchCommand extends AbstractCommand {

    @NotNull
    @Override
    public final String getName() {
        return "task-search";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Search for tasks by NAME or DESCRIPTION.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final String currentUserId = serviceLocator.getAppStateService().getUserId();
        if (currentUserId == null) throw new AccessDeniedException("Access denied");
        terminal.println("[TASK SEARCH]");
        terminal.println("SEARCH REQUEST");
        @Nullable final String searchRequest = terminal.readString();
        if (searchRequest == null || searchRequest.isEmpty())
            throw new IllegalSearchRequestException("Search request can't be empty");
        terminal.println("Select search type: NAME, DESCRIPTION, ALL");
        @Nullable final String typeSearch = terminal.readString();
        if (typeSearch == null || typeSearch.isEmpty())
            throw new IllegalSearchRequestException("Search request can't be empty");
        terminal.printTaskList(getTaskList(currentUserId, searchRequest, typeSearch));
    }

    @NotNull
    private Collection<Task> getTaskList(
            @NotNull final String userId,
            @NotNull final String searchRequest,
            @NotNull final String typeSearch
    ) {
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        switch (typeSearch.toLowerCase()) {
            case "name": return taskService.searchByName(userId, searchRequest);
            case "description": return taskService.searchByDescription(userId, searchRequest);
            case "all": {
                @NotNull final Collection<Task> searchByName =
                        taskService.searchByName(userId, searchRequest);
                @NotNull final Collection<Task> searchByDescription =
                        taskService.searchByDescription(userId, searchRequest);
                @NotNull final Set<Task> allResult = new HashSet<>();
                allResult.addAll(searchByName);
                allResult.addAll(searchByDescription);
                return allResult;
            }
            default: throw new IllegalSearchRequestException("invalid search type");
        }
    }

}