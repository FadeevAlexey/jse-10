package ru.fadeev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.exception.IllegalTaskNameException;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.api.service.ITaskService;

public final class TaskAddProjectCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-addProject";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Adding task to the project.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @Nullable final String currentUserId = serviceLocator.getAppStateService().getUserId();
        terminal.println("[ADD TASK TO PROJECT]");
        terminal.println("ENTER TASK NAME");
        @Nullable final String taskId = taskService.findIdByName(terminal.readString(), currentUserId);
        terminal.println("ENTER PROJECT NAME");
        @Nullable final String projectId = projectService.findIdByName(terminal.readString(), currentUserId);
        if (taskId == null || projectId == null) throw new IllegalTaskNameException("Can't find project or task");
        @Nullable final Task task = taskService.findOne(taskId);
        if (task == null) throw new IllegalTaskNameException("Can't find project or task");
        task.setProjectId(projectId);
        taskService.merge(task);
        terminal.println("[OK]\n");
    }

}