package ru.fadeev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.enumerated.Status;
import ru.fadeev.tm.exception.IllegalTaskNameException;
import ru.fadeev.tm.api.service.ITaskService;

import java.text.ParseException;
import java.util.Date;

public final class TaskEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit project.";
    }

    @Override
    public void execute() throws ParseException {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        terminal.println("[TASK EDIT]");
        terminal.println("ENTER CURRENT NAME:");
        @Nullable final String name = terminal.readString();
        @Nullable final String taskId = taskService.findIdByName(name, serviceLocator.getAppStateService().getUserId());
        if (taskId == null) throw new IllegalTaskNameException("Can't find task");
        @Nullable final Task task = taskService.findOne(taskId);
        if (task == null) throw new IllegalTaskNameException("Can't find task");
        fillFields(task);
        taskService.merge(task);
        terminal.println("[OK]\n");
        terminal.println("WOULD YOU LIKE ADD PROJECT TO TASK ? USE COMMAND task-addProject\n");
    }

    private void fillFields(@NotNull final Task task) throws ParseException {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        terminal.println("YOU CAN CHANGE PROPERTIES OR PRESS ENTER");
        terminal.println("CHOOSE AN STATUS DESIRED: Planned, In progress, Done");
        @Nullable String status = terminal.readString();
        if (status != null && !status.isEmpty()) task.setStatus(Status.getAllowableStatus(status));
        terminal.println("CHANGE DESCRIPTION:");
        @Nullable final String description = terminal.readString();
        terminal.println("CHANGE START DATE:");
        @Nullable final Date startDate = terminal.readDate();
        terminal.println("CHANGE FINISH DATE:");
        @Nullable final Date finishDate = terminal.readDate();
        if (description != null && !description.isEmpty()) task.setDescription(description);
        if (startDate != null) task.setStartDate(startDate);
        if (finishDate != null) task.setFinishDate(finishDate);
    }

}