package ru.fadeev.tm.command;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.text.ParseException;

public abstract class AbstractCommand {

    @Setter
    @NotNull
    protected ServiceLocator serviceLocator;

    public boolean isPermission(@Nullable final User user) {
        return user != null;
    }

    public abstract void execute() throws ParseException, IOException, ClassNotFoundException, JAXBException;

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    @Nullable
    public Role[] accessRole(){
        return null;
    }

}