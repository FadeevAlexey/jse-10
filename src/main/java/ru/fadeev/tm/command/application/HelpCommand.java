package ru.fadeev.tm.command.application;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.User;

public final class HelpCommand extends AbstractCommand {

    @Override
    public boolean isPermission(@Nullable final User user) {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        for (@NotNull final AbstractCommand abstractCommand : serviceLocator.getAppStateService().getCommands())
            terminal.println(String.format("%s: %s",abstractCommand.getName(),abstractCommand.getDescription()));
    }

}