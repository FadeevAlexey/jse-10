package ru.fadeev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.IllegalProjectNameException;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.api.service.ITaskService;

public final class ProjectTasksCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-tasks";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks inside project.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @Nullable final String currentUserId = serviceLocator.getAppStateService().getUserId();
        terminal.println("[PROJECT TASKS]");
        terminal.println("ENTER PROJECT NAME");
        @Nullable final String projectId = projectService.findIdByName(terminal.readString(), currentUserId);
        if (projectId == null) throw new IllegalProjectNameException("Can't find project");
        terminal.printTaskList(taskService.findAllByProjectId(projectId,currentUserId));
    }

}