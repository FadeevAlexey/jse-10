package ru.fadeev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.exception.IllegalProjectNameException;

public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final String userId =  serviceLocator.getAppStateService().getUserId();
        terminal.println("[PROJECT CREATE]");
        terminal.println("ENTER NAME:");
        @Nullable final String name = terminal.readString();
        if (name == null || name.isEmpty()) throw new IllegalProjectNameException("name can't be empty");
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        serviceLocator.getProjectService().persist(project);
        terminal.println("[OK]\n");
        terminal.println("WOULD YOU LIKE EDIT PROPERTIES PROJECT ? USE COMMAND project-edit\n");
    }

}