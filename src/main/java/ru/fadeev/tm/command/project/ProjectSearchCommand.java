package ru.fadeev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.IllegalSearchRequestException;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public final class ProjectSearchCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-search";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Search for projects by NAME or DESCRIPTION.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final String currentUserId = serviceLocator.getAppStateService().getUserId();
        if (currentUserId == null) throw new AccessDeniedException("Access denied");
        terminal.println("[PROJECT SEARCH]");
        terminal.println("SEARCH REQUEST");
        @Nullable final String searchRequest = terminal.readString();
        if (searchRequest == null || searchRequest.isEmpty())
            throw new IllegalSearchRequestException("Search request can't be empty");
        terminal.println("Select search type: NAME, DESCRIPTION, ALL");
        @Nullable final String typeSearch = terminal.readString();
        if (typeSearch == null || typeSearch.isEmpty())
            throw new IllegalSearchRequestException("Search request can't be empty");
        terminal.printProjectList(getProjectList(currentUserId, searchRequest, typeSearch));
    }

    @NotNull
    private Collection<Project> getProjectList(
            @NotNull final String userId,
            @NotNull final String searchRequest,
            @NotNull final String typeSearch
    ) {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        switch (typeSearch.toLowerCase()) {
            case "name":
                return projectService.searchByName(userId, searchRequest);
            case "description":
                return projectService.searchByDescription(userId, searchRequest);
            case "all": {
                @NotNull final Collection<Project> searchByName =
                        projectService.searchByName(userId, searchRequest);
                @NotNull final Collection<Project> searchByDescription =
                        projectService.searchByDescription(userId, searchRequest);
                @NotNull final Set<Project> allResult = new HashSet<>();
                allResult.addAll(searchByName);
                allResult.addAll(searchByDescription);
                return allResult;
            }
            default:
                throw new IllegalSearchRequestException("invalid search type");
        }
    }

}