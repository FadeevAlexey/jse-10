package ru.fadeev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.IllegalProjectNameException;
import ru.fadeev.tm.api.service.IProjectService;

public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @Nullable final String currentUserId = serviceLocator.getAppStateService().getUserId();
        terminal.println("[PROJECT REMOVE]\nENTER NAME");
        @Nullable final String projectId = projectService.findIdByName(terminal.readString(), currentUserId);
        if (projectId == null) throw new IllegalProjectNameException("Can't find project");
        projectService.remove(projectId);
        serviceLocator.getTaskService().removeAllByProjectId(projectId, currentUserId);
        terminal.println("[OK]\n");
    }

}