package ru.fadeev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.IllegalSortTypeException;

import java.util.Comparator;

public final class ProjectSortListCommand extends AbstractCommand {

    @NotNull
    private final String suffixRequest = "-DESC";

    @NotNull
    private final String prefixRequest = "NF ";

    @NotNull
    @Override
    public String getName() {
        return "project-sortList";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Shows sorted projects by START DATE, FINISH DATE, STATUS.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final String currentUserId = serviceLocator.getAppStateService().getUserId();
        if (currentUserId == null) throw new AccessDeniedException("Access denied");
        terminal.println("[PROJECT SORT LIST]");
        terminal.println("Select sorting type:\n" +
                "START DATE, FINISH DATE, STATUS or press ENTER for default sort by adding\n" +
                "if you'd like descending sort use suffix "+ suffixRequest +", for example: STATUS" +
                suffixRequest +"\n" + "if you'd like show null first use prefix " + prefixRequest +", for example: " +
                prefixRequest + "START DATE or " + prefixRequest + "START DATE"+ suffixRequest +" (only for date)");
        @Nullable final String sortRequest = terminal.readString();
        terminal.printProjectList(serviceLocator.getProjectService().findAll(currentUserId,getComparator(sortRequest)));
    }

    @Nullable
    private Comparator<Project> getComparator(@Nullable String selectedSort){
        if (selectedSort == null || selectedSort.isEmpty()) return null;
        @NotNull Comparator sortOrder = Comparator.naturalOrder();
        if (selectedSort.toUpperCase().contains(suffixRequest)) {
            sortOrder = Comparator.reverseOrder();
            selectedSort = selectedSort.substring(0,selectedSort.length() - suffixRequest.length());
        }
        @NotNull Comparator sortOptions = Comparator.nullsLast(sortOrder);
        if (selectedSort.toUpperCase().contains(prefixRequest)){
            sortOptions = Comparator.nullsFirst(sortOrder);
            selectedSort = selectedSort.substring(prefixRequest.length());
        }
        switch (selectedSort.toLowerCase()){
            case "start date":
                return Comparator.comparing(Project::getStartDate, sortOptions);
            case "finish date":
                return Comparator.comparing(Project::getFinishDate,sortOptions);
            case "status":
                return Comparator.comparing(Project::getStatus, sortOptions);
            default:
                throw new IllegalSortTypeException("cannot sort by type :" +selectedSort);
        }
    }

}