package ru.fadeev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;

public final class ProjectListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final String currentUserId = serviceLocator.getAppStateService().getUserId();
        terminal.println("[PROJECT LIST]");
        terminal.printProjectList(serviceLocator.getProjectService().findAll(currentUserId));
    }

}